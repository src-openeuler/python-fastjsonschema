%global _empty_manifest_terminate_build 0
%global pypi_name fastjsonschema

%global common_description %{expand:
fastjsonschema implements validation of JSON documents by JSON schema. 
The library implements JSON schema drafts 04, 06 and 07. 
The main purpose is to have a really fast implementation. 
}

Name:               python-%{pypi_name}
Version:            2.21.1
Release:            1
Summary:            Fastest Python implementation of JSON schema.
License:            BSD
URL:                https://github.com/horejsek/python-fastjsonschema
Source0:            https://github.com/horejsek/python-fastjsonschema/archive/refs/tags/%{name}-%{version}.tar.gz
BuildArch:          noarch


%description
%{common_description}

%package -n python3-%{pypi_name}
Summary:            %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}
BuildRequires:      python3-devel
BuildRequires:      python3-setuptools
BuildRequires:      python3-pbr
BuildRequires:      python3-pip
BuildRequires:      python3-wheel
	
# test dependencies
BuildRequires:      python3-mock
BuildRequires:      python3-pytest
BuildRequires:      python3-pytest-benchmark

%description -n python3-%{pypi_name}
%{common_description}

%prep
%autosetup -p1 -n %{name}-%{version}

# Remove upstreams egg info
rm -rf *.egg*

%build
%py3_build

%install
%py3_install

%check
CFLAGS="${CFLAGS:-${RPM_OPT_FLAGS}}" LDFLAGS="${LDFLAGS:-${RPM_LD_FLAGS}}" \
PATH="%{buildroot}%{_bindir}:$PATH" \
PYTHONPATH="${PYTHONPATH:-%{buildroot}%{python3_sitearch}:%{buildroot}%{python3_sitelib}}" \
PYTHONDONTWRITEBYTECODE=1 \
%{?__pytest_addopts:PYTEST_ADDOPTS="${PYTEST_ADDOPTS:-} %{__pytest_addopts}"} \
PYTEST_XDIST_AUTO_NUM_WORKERS=%{_smp_build_ncpus} \
%{_bindir}/pytest

%files -n python3-%{pypi_name}
%doc README.rst
%license LICENSE
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-*.egg-info/

%changelog
* Mon Dec 23 2024 sqfu <dev01203@linx-info.com> - 2.21.1-1
- update to 2.21.1
- Fix date pattern regexp
- Add re import when using regex pattern
- Add detailed_exception parameter

* Mon Feb 26 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 2.19.1-1
- Update package to version 2.19.1
  Fix date format

* Tue Dec 05 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 2.19.0-1
- Update package to version 2.19.0

* Mon Jul 10 2023 Dongxing Wang <dxwangk@isoftstone.com> - 2.17.1-1
- Initial packaging
